FROM python:3.9

WORKDIR /app

COPY todo_app.py .

CMD ["python", "todo_app.py"]
